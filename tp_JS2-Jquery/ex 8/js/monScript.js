$(document).ready(function () {
    
    var x = 1;                                            //Nombre de lignes
    var y = 1;                                            //Resultat de l'addition des cases
    var etat = 1;                                         //1 = addition / 0 = soustraction
    function calculTotal() {                              //Fonction de calcul
        if(etat == 1)                                     //Si on est dans l'addition
        {
            y = y + x;                                    //y = sa valeur précédente + le numéro de la case
        }
        else                                              //Si on est dans la soustraction
        {
            y = y - x;                                    //y = sa valeur précédente - le numéro de la case
        }
        $("#resultat td:first-child").html(y);            //Affichage du résultat
    }
    $("#body td:first-child").html(x);                    //Affichage de la première case
    $("#resultat td:first-child").html(y);                //Affichage du resultat
    $("#btn1").click(function() {                         //Si on clique si le btn1
        $("#body tr:nth-child(" + x + ")").after("<tr><td>" + $("td").length + "</td></tr>");
            //Après la ligne n°x, on ajoute une ligne contenant le nombre de lignes présentes
        x = x + 1;                                        //On ajoute une ligne
        etat = 1;                                         //On est dans l'addition
        calculTotal();                                    //Appel de la fonction
    });
    
    $("#btn2").click(function() {                         //Si on clique sur le btn2
        if (x > 1)                                        //Contrôle pour ne pas supprimer la première case
        {
            $("#body tr:nth-child(" + x + ")").remove();  //la ligne n°x est supprimée
            etat = 0;                                     //On est dans la soustraction
            calculTotal();                                //Appel de la fonction
            x = x - 1;                                    //On enlève une ligne
        }
    });
});