$(document).ready(function () {
    $("#maDiv1").hide();                            //Cacher la div
    var x = 0;                                      //Etat de la div : cachée
    $("#btn1").click(function() {                   //Lorsqu'on clique sur le bouton
        if (x == 0)                                 //Si l'état est : cachée
        {
            $("#maDiv1").show();                    //Afficher la div
            $("#btn1").attr("value", "Fermer");     //le texte du bouton change
            x = 1;                                  //L'état passe à affichée
        }
        else                                        //Si l'état est : affichée
        {
            $("#maDiv1").hide();                    //Cacher la div
            $("#btn1").attr("value", "Ouvrir");     //Le texte du bouton change
            x = 0;                                  ////L'état passe à cachée
        }
    });
});