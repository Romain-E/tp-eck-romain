$(document).ready(function () {
    
    var x = 1;                                              //Nombre de lignes
    $("#btn1").click(function() {                           //Si on clique si le btn1
        $("#ligne tr:nth-child(" + x + ")").after("<tr><td>" + $("tr").length + "</td></tr>");
            //Après la ligne n°x, on ajoute une ligne contenant le nombre de lignes présentes
        x = x + 1;                                          //On ajoute une ligne
    });
    
    $("#btn2").click(function() {                           //Si on clique sur le btn2
        if (x > 1)                                          //Contrôle pour ne pas supprimer la première case
        {
            $("#ligne tr:nth-child(" + x + ")").remove();   //la ligne n°x est supprimée
            x = x - 1;                                      //On enlève une ligne
        }
    });
});