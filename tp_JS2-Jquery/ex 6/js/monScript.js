$(document).ready(function () {
    
    var x = 1;                                              //Nombre de lignes
    $("#btn1").click(function() {                           //Si on clique si le btn1
        $("#ligne tr:nth-child(" + x + ")").after("<tr><td>1</td></tr>");
            //Après la ligne n°x, on ajoute un ligne contenant le chiffre 1
        x = x + 1;                                          //On ajoute une ligne
    });
    
    $("#btn2").click(function() {                           //Si on clique sur le btn2
        if (x > 1)                                          //Contrôle pour ne pas supprimer la première case
        {
            $("#ligne tr:nth-child(" + x + ")").remove();   //On supprime la ligne n°x
            x = x - 1;                                      //On enlève une ligne
        }
    });
});