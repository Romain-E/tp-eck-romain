var a = 0;                                                //Années
var mois = 0;                                             //Mois
var j = 0;                                                //Jours
var h = 0;                                                //Heures
var m = 0;                                                //Minutes
var s = 0;                                                //Secodes
var r = 0;                                                //Reste
var r1 = 0;                                               //Reste 1
var r2 = 0;                                               //Reste 2
var r3 = 0;                                               //Reste 3 
var x = prompt("nombre de senconde(s)");                  //Nb de secondes demandées par l'utilisateur

if (x < 60)                                               // Si x est < 1 minute
{
    document.write(x + " secondes est égal à " + x + " seconde(s)");
}

if ((x >= 60) && (x < 3600))                              // Si x est entre 1 miute et 59 minute et 59 secondes
{
    m = Math.trunc(x / 60);                                //Nb de minutes
    s = x % 60;                                           // Nb de secondes
    document.write(x + " secondes est égal à " + m + " minute(s) et " + s + " seconde(s).");
}

if ((x >= 3600) && (x < 86400))                           // Si x est entre 1 heures et 23 heures 59 minutes et 59 secondes
{
    h = Math.trunc(x / 3600);                              //Nb d'heures
    r = x % 3600;                                         // Nb de secondes restantes

    if ((r >= 60) && (r < 3600))                          // Si r est entre 1 miute et 59 minute et 59 secondes
    {
        m = Math.trunc(r / 60);                            //Donne le nb de minutes
        s = r % 60;                                       // Donne le nb de secondes
    }else{
        s = r;                                            // Donne le nb de secondes si r < 1 minute
    }
    document.write(x + " secondes est égal à " + h + " heure(s), " + m + " minute(s) et " + s + " seconde(s).");
}

if ((x >= 86400) && (x < 2592000))                         // Si x est entre 1 jours et 29 jours 23 heures 59 minutes et 59 secondes
{
    j = Math.trunc(x / 86400);                              //Donne le nb de jours
    r = x % 86400;                                         // Donne le nb de secondes restantes

    h = Math.trunc(r / 3600);                               //Nb d'heures
    r1 = r % 3600;                                         // Nb de secondes restantes

    if ((r1 >= 60) && (r1 < 3600))                         // Si r1 >= 1 miute
    {
        m = Math.trunc(r1 / 60);                            //Donne le nb de minutes
        s = r1 % 60;                                       // Donne le nb de secondes
    }else{
        s = r1;                                            // Donne le nb de secondes si r1 < 1 minute
    }
    document.write(x + " secondes est égal à " + j + " jour(s), " + h + " heure(s), " + m + " minute(s) et " + s + " seconde(s).");
}

if ((x >= 2592000) && (x < 31536000))                      // Si x est entre 1 mois et 11 mois 29 jours 23 heures 59 minutes et 59 secondes
{
    mois = Math.trunc(x / 2592000);                         //Donne le nb de mois
    r = x % 2592000;                                       // Donne le nb de secondes restantes

    j = Math.trunc(r / 86400);                              //Donne le nb de jours
    r1 = r % 86400;                                        // Donne le nb de secondes restantes

    h = Math.trunc(r1 / 3600);                              //Nb d'heures
    r2 = r1 % 3600;                                        // Nb de secondes restantes

    if (r2 >= 60)                                          // Si r2 >= 1 minute
    {
        m = Math.trunc(r2 / 60);                            //Donne le nb de minutes
        s = r2 % 60;                                       // Donne le nb de secondes
    }else{
        s = r2;                                            // Donne le nb de secondes si r2 < 1 minute
    }
    document.write(x + " secondes est égal à " + mois + " mois, " + j + " jour(s), " + h + " heure(s), " + m + " minute(s) et " + s + " seconde(s).");
}

if (x >= 31536000)                                         //  Si x est superieur à un an
{
    a = Math.trunc(x / 31536000);                           // Donne le nb d'année
    r = x % 31536000;                                      //  Donne le nb de secondes restantes

    mois = Math.trunc(r / 2592000);                         // Donne le nb de mois
    r1 = r % 2592000;                                      //  Donne le nb de secondes restantes

    j = Math.trunc(r1 / 86400);                             // Donne le nb de jours
    r2 = r1 % 86400;                                       //  Donne le nb de secondes restantes

    h = Math.trunc(r2 / 3600);                               //Nb d'heures
    r3 = r2 % 3600;                                         // Nb de secondes restantes
    if (r3 >= 60)                                          //  Si r3 > 1 miute
    {
        m = Math.trunc(r3 / 60);                            // Donne le n de minutes
        s = r3 % 60;                                       //  Donne le nb de secondes
    }else{
        s = r3;                                            //  Donne le nombre de seconde si r3 < 1 minute
    }
    document.write(x + " secondes est égal à " + a + " année(s), " + mois + " mois, " + j + " jour(s), " + h + " heure(s), " + m + " minute(s) et " + s + " seconde(s).");
}