$(document).ready(function () {

    function calculerIMC(prmPoids, prmTaille){                      //Fonction calcul d'IMC
        prmTaille = prmTaille / 100;                                //Conversion cm en m
        var valRetour = prmPoids / (prmTaille * prmTaille)          //Calcul de l'IMC
        return valRetour;
    }

    function interpreterIMC(prmValImc){                             //Fonction interpretation
        var interpretation = "";
        if (prmValImc < 16.5) {
            interpretation = "dénutrition";
        }
        if ((prmValImc >= 16.5) && (prmValImc < 18.5)) {
            interpretation = "maigreur";
        }
        if ((prmValImc >= 18.5) && (prmValImc < 25)) {
            interpretation = "corpulence normale";
        }
        if ((prmValImc >= 25) && (prmValImc < 30)) {
            interpretation = "surpoids";
        }
        if ((prmValImc >= 30) && (prmValImc < 35)) {
            interpretation = "obésité modérée";
        }
        if ((prmValImc >= 35) && (prmValImc < 40)) {
            interpretation = "obésité sévère";
        }
        if (prmValImc >= 40) {
            interpretation = "obésité morbide";
        }
        return interpretation;
    }

    function afficherIMC() {                                        //Fonction affichage de l'IMC
        var poids = $("#idSliderPoids").val();                      //Lecture du poids
        $("#textPoids").html(poids);                                //Affichage de la valeur du poids
        var taille = $("#idSliderTaille").val();                    //Lecture de la taille
        $("#textTaille").html(taille);                              //Affichage de la valeur de la taille
        var imc = calculerIMC(poids, taille);                       //Calcul de l'IMC
        var inter = interpreterIMC(imc);                            //Interpretatin de l'IMC
        $("#textIMC").html(imc.toFixed(1) + " (" + inter + ")");    //Affichage de l'IMC et de l'interprétation
    }

    afficherIMC();                                                  //Affichage de l'IMC pour les valeurs par défaut
    
    $("#idSliderPoids").on('input', function () {                   //Déplacement du curceur poids
        afficherIMC()                                               //Appel fonction d'affichage
    });    

    $("#idSliderTaille").on('input', function () {                  //Déplacement du curceur taille
        afficherIMC()                                               //Appel fonction d'affichage
    });  
    
});