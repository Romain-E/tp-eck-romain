$(document).ready(function () {

    function calculerIMC(prmPoids, prmTaille) {                     //Fonction calcul d'IMC
        prmTaille = prmTaille / 100;                                //Conversion cm en m
        var valRetour = prmPoids / (prmTaille * prmTaille);         //Calcul de l'IMC
        return valRetour;
    }

    function interpreterIMC(prmValImc) {                             //Fonction interpretation
        var interpretation = "";
        if (prmValImc < 16.5) {
            interpretation = "dénutrition";
        }
        if ((prmValImc >= 16.5) && (prmValImc < 18.5)) {
            interpretation = "maigreur";
        }
        if ((prmValImc >= 18.5) && (prmValImc < 25)) {
            interpretation = "corpulence normale";
        }
        if ((prmValImc >= 25) && (prmValImc < 30)) {
            interpretation = "surpoids";
        }
        if ((prmValImc >= 30) && (prmValImc < 35)) {
            interpretation = "obésité modérée";
        }
        if ((prmValImc >= 35) && (prmValImc < 40)) {
            interpretation = "obésité sévère";
        }
        if (prmValImc >= 40) {
            interpretation = "obésité morbide";
        }
        return interpretation;
    }

    function afficherBalance(prmValImc) {                           //Fonction affichage de la balance
        if ((prmValImc >= 10) && (prmValImc <= 45)) {               //IMC en 10 et 45
            var deplacement = ((60/7) * prmValImc) -(600/7);        //Calcul du déplacement de l'aiguille
            $("#aiguille").css("left", deplacement + "px");         //Déplacement de l'aiguille
        }
    }
    function afficherSilhouette(prmValImc) {                        //Fonction affichage de la silhouette
        var sexe = $("input[name=sexe]:checked").val();             //Lecture de la valeur du bouton coché
        if (sexe == "homme") {                                      //Si la valeur est "homme"
            $("#silhouette").css("background-image", 
            "url(css/img/IMC-homme.jpg)");                          //On choisi l'image homme
        }else{                                                      //Si la valeur est femme
            $("#silhouette").css("background-image", 
            "url(css/img/IMC-femme.jpg)");                          //On choisi l'image femme
        }
        var decalage = "";
        if (prmValImc == "dénutrition") {
            decalage = 630;
        }
        if (prmValImc == "maigreur") {
            decalage = 525;
        }
        if (prmValImc == "corpulence normale") {
            decalage = 420;
        }
        if (prmValImc == "surpoids") {
            decalage = 315;
        }
        if (prmValImc == "obésité modérée") {
            decalage = 210;
        }
        if (prmValImc == "obésité sévère") {
            decalage = 105;
        }
        if (prmValImc == "obésité morbide") {
            decalage = 105;
        }
        $("#silhouette").css("background-position", decalage);
    }

    function afficherIMC() {                                        //Fonction affichage de l'IMC
        var poids = $("#idSliderPoids").val();                      //Lecture du poids
        $("#textPoids").html(poids);                                //Affichage du poids
        var taille = $("#idSliderTaille").val();                    //Lecture de la taille
        $("#textTaille").html(taille);                              //Affichage du poids
        var imc = calculerIMC(poids, taille);                       //Calcul de l'IMC
        var inter = interpreterIMC(imc);                            //Interpretatin de l'IMC
        $("#textIMC").html(imc.toFixed(1) + " (" + inter + ")");    //Affichage de l'IMC et de l'interprétation
        afficherBalance(imc);                                       //Affichage de la balance
        afficherSilhouette(inter)                                   //Affichage de la silhouette
    }

    afficherIMC();                                                  //Affichage de l'IMC pour les valeurs par défaut

    $("input[name=sexe]").on('change', function () {                //Sélection du sexe
        afficherIMC();                                              //Appel fonction d'affichage
    });
    
    $("#idSliderPoids").on('input', function () {                   //Déplacement du curceur poids
        afficherIMC()                                               //Appel fonction d'affichage
    });    

    $("#idSliderTaille").on('input', function () {                  //Déplacement du curceur taille
        afficherIMC();                                              //Appel fonction d'affichage
    });  
    
});