$(document).ready(function () {

    function calculerIMC(prmPoids, prmTaille){                 //poids en kg et taille en mètre
        prmTaille = prmTaille / 100;                           //Conversion cm en m
        var valRetour = prmPoids / (prmTaille * prmTaille);    //Calcul de l'IMC
        return valRetour;
    }

    function interpreterIMC(prmValImc){                        //Fonction interpretation
        var interpretation = "";
        if (prmValImc < 16.5) {
            interpretation = "dénutrition";
        }
        if ((prmValImc >= 16.5) && (prmValImc < 18.5)) {
            interpretation = "maigreur";
        }
        if ((prmValImc >= 18.5) && (prmValImc < 25)) {
            interpretation = "corpulence normale";
        }
        if ((prmValImc >= 25) && (prmValImc < 30)) {
            interpretation = "surpoids";
        }
        if ((prmValImc >= 30) && (prmValImc < 35)) {
            interpretation = "obésité modérée";
        }
        if ((prmValImc >= 35) && (prmValImc < 40)) {
            interpretation = "obésité sévère";
        }
        if (prmValImc >= 40) {
            interpretation = "obésité morbide";
        }
        return interpretation;
        }
    
    $("#btnCalculImc").click(function() {                      //Clique sur le bouton

        var poids = $("#idPoids").val();                       //Lecture de la valeur du poids
        var taille = $("#idTaille").val();                     //Lecture de la valeur de la taille

        poids = poids.replace(",", ".");                       //Remplacement des virgules par des points
        poids = Number(poids);                                 //Conversion en nombre des valeurs saisie
        
        if (isNaN(poids) || isNaN(taille)) {                   //Si les valeurs saisie ne sont pas de nombres
            alert("Saisie incorrecte");                        //Affichage du message d'erreur
        }else {
            
            var imc = calculerIMC(poids, taille);              //Appel de la fonction
            var inter = interpreterIMC(imc);
            $("#textIMC").html(imc.toFixed(1) + " (" + inter + ")");  //Affiche du résultat, 1 chiffre après la virgule avec interprétation
        }
    });
});