$(document).ready(function () {

    function calculerIMC(prmPoids, prmTaille){                 //poids en kg et taille en mètre

        prmTaille = prmTaille / 100;                           //Conversion cm en m
        var valRetour = prmPoids / (prmTaille * prmTaille);    //Calcul de l'IMC
        return valRetour;
        }

    $("#btnCalculImc").click(function() {                      //Clique sur le bouton

        var poids = $("#idPoids").val();                       //Lecture de la valeur du poids
        var taille = $("#idTaille").val();                     //Lecture de la valeur de la taille

        poids = poids.replace(",", ".");                       //Remplacement des virgules par des points
        poids = Number(poids);                                 //Conversion en nombre des valeurs saisie
        
        if (isNaN(poids) || isNaN(taille)) {                   //Si les valeurs saisie ne sont pas de nombres
            alert("Saisie incorrecte");                        //Affichage du message d'erreur
        }else {
            
            var imc = calculerIMC(poids, taille);              //Appel de la fonction
            $("#textIMC").html(imc.toFixed(1));                //Affiche du résultat, 1 chiffre après la virgule
        }
    });
});